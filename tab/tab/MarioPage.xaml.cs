﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tab

{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MarioPage : ContentPage
	{
		public MarioPage ()
		{
			InitializeComponent ();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(MarioPage)}:  ctor");
        }

        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }
    }
}